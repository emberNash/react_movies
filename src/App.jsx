import React from 'react';
import { Header } from './layout/Header'
import { Footer } from './layout/Footer'
import { MainComponent } from './layout/MainComponent'
function App() {
	return (
		<React.Fragment>
			<Header></Header>
			<MainComponent></MainComponent>
			<Footer></Footer>
		</React.Fragment>
	);
}

export default App;
