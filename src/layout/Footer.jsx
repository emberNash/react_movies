function Footer() {
	return (
		<footer className="page-footer  lime darken-1">
			<div className="footer-copyright">
				<div className="container">
					© {new Date().getFullYear()} Copyright Text
					<a className="grey-text text-lighten-4 right" href="#!">Pepo</a>
				</div>
			</div>
		</footer>
	)
}

export { Footer }