import React, {useEffect, useState} from 'react';
import {Movies} from '../components/Movies';
import {Preloader} from '../components/Preloader';
import {Search} from '../components/Search';

const API_KEY = process.env.REACT_APP_API_KEY;

function MainComponent() {
    const [movies, setMovies] = useState([]);
    const [loading, setLoading] = useState(+true);

    const searchMovies = (str, type = 'all') => {
        setLoading(+true);
        fetch(
            `http://www.omdbapi.com/?apikey=${API_KEY}&s=${str}${
                type !== 'all' ? `&type=${type}` : ''
            }`
        )
            .then((response) => response.json())
            .then((data) => {
                setMovies(data.Search);
                setLoading(+false);
            })
            .catch((err) => {
                setLoading(+false);
            });
    };


    useEffect(() => {
        fetch(`http://www.omdbapi.com/?apikey=${API_KEY}&s=matrix`)
            .then((response) => response.json())
            .then((data) => {
                setMovies(data.Search);
                setLoading(+false);
            })
            .catch((err) => {
                setLoading(+false);
            });
    }, []);

    return (
        <main main className='container content'>
            <Search searchMovies={searchMovies}></Search>
            {loading === +true ? (
                <Preloader></Preloader>
            ) : (
                <Movies movies={movies}></Movies>
            )}
        </main>
    );
    console.log('test commit')
}

export { MainComponent };
